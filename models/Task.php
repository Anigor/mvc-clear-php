<?

class Task
{
	private $limit = 3;
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

	public function addTask($name, $email, $task)
	{
		$query  = 'INSERT INTO `test_task`(`user_name`, `email`, `task`) VALUES ("'.$name.'", "'.$email.'", "'.$task.'")';
 		$this->db->query($query);
	}

	public function updateTask($idd, $task_text, $task_status)
	{
		$query  = 'UPDATE `test_task` SET `task`="'.$task_text.'",`status`='.$task_status.' WHERE `id` = '.$idd;
 		$this->db->query($query);
	}

	public function getCountTasks()
	{
		$query = "SELECT count(*) as count FROM `test_task`";
		$result = $this->db->query($query);
		return $result->fetch_array(MYSQLI_ASSOC);
	}

	public function getPageTasks($offset = 0, $sortParam = "user_name", $sortType = "ASC")
	{
		$data = array();
	    $query = "SELECT * 
	    		  FROM `test_task` 
	    		  ORDER BY ".$sortParam." ".$sortType." 
	    		  LIMIT ".$this->limit*$offset.", ".$this->limit;
		$result = $this->db->query($query);
	    for ($i=0; $i < $result->num_rows; $i++) { 
	    	$data[] = $result->fetch_array(MYSQLI_ASSOC);
	    }
		return $data;
	}
}
