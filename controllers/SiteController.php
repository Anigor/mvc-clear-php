<?

class SiteController
{
	private $view;
	private $db;
	
	function __construct($View, $db)
	{
		$this->$view = $View;
		$this->db = $db;
	}

	function getFormData() {
		$data = (object) array();
		$data = json_decode(file_get_contents('php://input'), true);
	    return $data;
	}

	public function index()
	{
	    $page = (int)$_GET['page'];
	    
		$modelTask = new Task($this->db);
		$total = $modelTask->getCountTasks();
		$paginationParam = "";
		if ($_GET['action'] == "sort"){
			$sortParam = rtrim(htmlspecialchars($_GET['param']));
			if(!in_array($sortParam, array('user_name', 'email', 'status'))){
				$sortParam = "user_name";
			}
			$sortType = (rtrim(htmlspecialchars($_GET['type'])) == "DESC" ? "DESC" : "ASC");
			$paginationParam = "&action=sort&param=".$sortParam."&type=".$sortType;

			$tasks = $modelTask->getPageTasks($page, $sortParam, $sortType);
		}else{
			$tasks = $modelTask->getPageTasks($page);
		}

		$kol = 3;
	    $art = ($page * $kol);
	    $countPage = ceil($total['count'] / $kol);
		
        $this->$view->index(array("title" => "Задачи", 
        						  "tasks" => $tasks, 
        						  "num_start" => $page,
        						  "admin" => (isset($_SESSION['login']) ? 1 : 0),
        						  "countPage" => $countPage,
        						  "paginationParam" => $paginationParam));
	}

	public function login()
	{
		$out['error'] = 0;
		$data = $this->getFormData();
		$login = rtrim(htmlspecialchars($data['login']));
		$password = rtrim(htmlspecialchars($data['password']));
		if ($login == "admin" and $password == "123"){
			$out['error'] = 0;
			$_SESSION['login'] = 1;
		}else{
			$out['error'] = 1;
		}

		echo json_encode($out);
	}

	public function Logout()
	{
		$out['error'] = 0;
		unset($_SESSION['login']);
		echo json_encode($out);
	}

	public function AddTask()
	{
		$out['error'] = 0;
		$data = $this->getFormData();

		$userName = rtrim(htmlspecialchars($data['name']));
		$userEmail = rtrim(htmlspecialchars($data['email']));
		$task = rtrim(htmlspecialchars($data['task']));
		if($userName == ""){
			$out['errorName'] = "Поле не может быть пустым";
			$out['error'] = 1;
		}
		if($userEmail == ""){
			$out['errorEmail'] = "Поле не может быть пустым";
			$out['error'] = 1;
		}
		if($task == ""){
			$out['errorTask'] = "Поле не может быть пустым";
			$out['error'] = 1;
		}
		if($out['error'] == 0){
			try {
				$modelTask = new Task($this->db);
				$modelTask->addTask($userName, $userEmail, $task);
			} catch (Exception $e) {
				$out['error'] = 1;
				$out['text'] = "Ошибка добавления задания";
			}
		}
		echo json_encode($out);
	}

	public function UpdateTask()
	{
		$out['error'] = 1;
		$data = $this->getFormData();

		$task_id = (int)$data['idd'];
		$task_status = (int)$data['status'];
		$task_text = rtrim(htmlspecialchars($data['task_text']));
		if($task_id > 0 && $task_text != "" && $_SESSION['login'] == 1)
		{
			try {
				$modelTask = new Task($this->db);
				$modelTask->updateTask($task_id, $task_text, $task_status);
				$out['error'] = 0;
			} catch (Exception $e) {
				$out['text'] = "Ошибка добавления задания";
			}
		}
		echo json_encode($out);
	}
}
