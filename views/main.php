<?

class View
{
    function showHead($title)
    {
        echo '
            <!DOCTYPE html>
            <html>
            <head>
            	<title>'.$title.'</title>
            	<link href="/include/css/bootstrap.css" rel="stylesheet">
                <script src="/include/js/jquery-3.6.0.min.js"></script>
            	<script src="/include/js/bootstrap.js"></script>
                <script src="/include/js/script.js"></script>
            </head>
            <body>';
    }
    
    function showMenu($page, $admin)
    {
        echo '
            <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
                <h5 class="my-0 mr-md-auto font-weight-normal">
                    <a href="/">Главная</a>
                </h5>
                '.($admin == 0 ? '<a class="btn btn-outline-primary btn-login" data-bs-toggle="modal" data-bs-target="#loginModal" href="#">Войти</a>' : '<a class="btn btn-outline-primary logout btn-login" href="#">Выйти</a>').'
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#AddTaskModal">Создать задачу</button>
                    </div>
                    <div class="col-md-12">
                        <table class="table table-hover">
                            <tr>
                                <th>№</th>
                                <th>Имя</th>
                                <th>Email</th>
                                <th>Задача</th>
                                <th>Статус</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <a href="/?page='.$page.'&action=sort&param=user_name&type=ASC" class="btn btn-sm btn-secondary">/\</a>
                                    <a href="/?page='.$page.'&action=sort&param=user_name&type=DESC" class="btn btn-sm btn-secondary">\/</a>
                                </td>
                                <td>
                                    <a href="/?page='.$page.'&action=sort&param=email&type=ASC" class="btn btn-sm btn-secondary">/\</a>
                                    <a href="/?page='.$page.'&action=sort&param=email&type=DESC" class="btn btn-sm btn-secondary">\/</a>
                                </td>
                                <td></td>
                                <td>
                                    <a href="/?page='.$page.'&action=sort&param=status&type=ASC" class="btn btn-sm btn-secondary">/\</a>
                                    <a href="/?page='.$page.'&action=sort&param=status&type=DESC" class="btn btn-sm btn-secondary">\/</a>
                                </td>
                                <td></td>
                            </tr>
                            ';
    }
    
    function showFutter()
    {
        echo '
                </div>
    
                <div class="modal fade" id="AddTaskModal" tabindex="-1" aria-labelledby="AddTaskModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="AddTaskModalLabel">Создать задачу</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        <form class="FromAddTask">
                          <div class="mb-3">
                            <label for="user_name" class="form-label">Имя пользователя</label>
                            <input type="text" class="form-control" name="name" id="user_name">
                            <div id="error_name" class="form-text"></div>
                          </div>
                          <div class="mb-3">
                            <label for="user_email" class="form-label">Email пользователя</label>
                            <input type="email" class="form-control" name="email" id="user_email" aria-describedby="emailHelp">
                            <div id="error_email" class="form-text"></div>
                          </div>
                          <div class="mb-3">
                            <label for="user_task" class="form-label">Задача</label>
                            <textarea rows="2" class="form-control" name="task" id="user_task"></textarea>
                            <div id="error_text" class="form-text"></div>
                          </div>
                          
                          <button type="submit" class="btn btn-primary">Создать задачу</button>
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="loginModal" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="loginModalLabel">Modal title</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <div class="modal-body">
                        <form class="FromLogin">
                          <div class="mb-3">
                            <label for="user_name" class="form-label">Логин</label>
                            <input type="text" class="form-control" name="login" id="user_name">
                          </div>
                          <div class="mb-3">
                            <label for="user_password" class="form-label">Пароль</label>
                            <input type="password" class="form-control" name="password" id="user_password">
                            <div id="error_login" class="form-text"></div>
                          </div>
                          
                          <button type="submit" class="btn btn-primary">Авторизоваться</button>
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Отмена</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

            </body>
        </html>';
    }
    
	public function index($arguments)
	{
        $num = (int)$arguments['num_start'];
        $admin = $arguments['admin'];

	    $this->showHead($arguments['title']);
	    $this->showMenu($num, $admin);
        
        $num *= 3; 
        foreach ($arguments['tasks'] as $value) {
            $num++;
            echo '
                <tr>
                    <td>'.$num.'</td>
                    <td>'.$value['user_name'].'</td>
                    <td>'.$value['email'].'</td>
                    <td>'.($admin == 0 ? $value['task'] : '<textarea class="form-control" id="text_task_'.$value['id'].'">'.$value['task'].'</textarea>' ).'</td>';
                    if($admin == 0) {
                        echo '<td>'.($value['status'] == 0 ? "В работе" : "Выполнено").'</td>';
                    }else{
                        echo '
                            <td>
                                <select class="form-select" id="select_'.$value['id'].'">
                                    <option value="0" '.($value['status'] == 0 ? 'selected' : '').'>В работе</option>
                                    <option value="1" '.($value['status'] == 1 ? 'selected' : '').'>Выполнено</option>
                                </select>
                            </td>';
                    }
                echo '<td>'.($admin == 0 ? '' : '<span class="btn btn-info update_task" data-val="'.$value['id'].'">Изменить</span>').'</td>
                </tr>';
        }
        echo '
                </table>
            </div>
            <div class="col-md-12">';
        for ($i = 0; $i < $arguments['countPage']; $i++){
            echo '<a class="btn btn-primary" href="/?page='.$i.$arguments['paginationParam'].'"> '.($i+1).' </a>';
        }
        echo '</div>
        </div>';
	    $this->showFutter();
		
	}
	
}