<?
$base_puth = $_SERVER['DOCUMENT_ROOT'];

function loadClass($puth){
	$skip = array('.', '..', 'index.php');
	$files = scandir($puth);
	foreach($files as $file) {
	    if(!in_array($file, $skip)){
	    	if(is_dir($puth.'/'.$file)){
	    		loadClass($puth.'/'.$file);
	    	}
	    	if(is_file($puth.'/'.$file)){
	    		$file_type = end(explode(".", $file));
	    		if($file_type == "php"){
	    			include $puth.'/'.$file;
	    		}
	    	}
	    }
	}
}
loadClass($base_puth);

if(session_status() !== PHP_SESSION_ACTIVE) session_start();

$db = new mysqli("localhost", "host1763385_test", "UvGH60be", "host1763385_test");

if (!$db->connect_error) {
	mysqli_set_charset ($db, 'utf8mb4');
	$view = new View();

	$site = new SiteController($view, $db);
	$action = rtrim(htmlspecialchars($_GET['action']));
	switch ($action) {
		case 'addTask':
			$site->addTask();
			break;

		case 'login':
			$site->login();
			break;

		case 'logout':
			$site->logout();
			break;

		case 'update':
			$site->updateTask();
			break;

		case 'sort':
			$site->index();
			break;
		
		default:
			$site->index();
			break;
	}

    $db->close();
}
