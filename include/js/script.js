$(document).ready(function (){
	
	$(document).on('submit', 'form.FromAddTask', function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        formData.append('action', 'addTask');
		console.log(formData);
        var object = {};
        formData.forEach(function(value, key){
            object[key] = value;
        });

        fetch('/?action=addTask', {
            method: 'post',
            credentials: 'same-origin',
            headers: {"Content-type": "application/json"},
            body: JSON.stringify(object)
        }).then(function(response){
            return response.json();
        }).then(function(data) {
            if (data.error == 1) {
                $('#error_name').html(data.errorName);
                $('#error_email').html(data.errorEmail);
                $('#error_text').html(data.errorTask);
            }else{
               $("#AddTaskModal").modal("hide");
               location.reload();
            }                
        }).catch(function(error){
            
        });
        
    });

    $(document).on('submit', 'form.FromLogin', function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        formData.append('action', 'login');

        var object = {};
        formData.forEach(function(value, key){
            object[key] = value;
        });
       
        fetch('/?action=login', {
            method: 'post',
            credentials: 'same-origin',
            headers: {"Content-type": "application/json"},
            body: JSON.stringify(object)
        }).then(function(response){
            return response.json();
        }).then(function(data) {
            if (data.error == 1) {
                $('#error_login').html('Не правильный логин и/или пароль');
            }else{
               location.reload();
            }                
        }).catch(function(error){
            
        });
        
    });

    $(document).on('click', '.logout', function(e) {
        e.preventDefault();

        fetch('/?action=logout', {
            method: 'post',
            credentials: 'same-origin',
            headers: {"Content-type": "application/json"},
            body: ""
        }).then(function(response){
            return response.json();
        }).then(function(data) {
            if (data.error != 1) {
               location.reload();
            }                
        }).catch(function(error){
            
        });
    });

    $(document).on('click', '.update_task', function(e) {
        e.preventDefault();
        
        var object = {};
        object['idd'] = $(this).data('val');
        object['task_text'] = $('#text_task_'+object['idd']).val();
        object['status'] = $('#select_'+object['idd']).val(); 
       
        fetch('/?action=update', {
            method: 'post',
            credentials: 'same-origin',
            headers: {"Content-type": "application/json"},
            body: JSON.stringify(object)
        }).then(function(response){
            return response.json();
        }).then(function(data) {
            if (data.error != 1) {
               location.reload();
            }                
        }).catch(function(error){
            
        });
    });
    

});
